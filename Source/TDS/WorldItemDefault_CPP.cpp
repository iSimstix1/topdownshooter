// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldItemDefault_CPP.h"

// Sets default values
AWorldItemDefault_CPP::AWorldItemDefault_CPP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWorldItemDefault_CPP::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWorldItemDefault_CPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

